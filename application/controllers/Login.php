<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        // carrega os recursos necessários
        $this->load->library('google_recaptcha');

        // configura o Google reCAPTCHA
        $this->google_recaptcha->setSiteKey("**************************");
        $this->google_recaptcha->setSecretKey("**************************");

        // verifica o Google reCAPTCHA
        if ($this->input->post('g-recaptcha-response') != false) {
            $this->google_recaptcha->setResponse($this->input->post('g-recaptcha-response'));

            if ($this->google_recaptcha->validateResponse()) {
                // está válido
            } else {
                // inválido
            }
        }

        $this->load->view('login');
    }

}
