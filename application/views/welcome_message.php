<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Welcome to CodeIgniter</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
		<link href="<?= base_url('assets/style.css') ?>" rel="stylesheet">
  	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		
		
	</head>
	<body>

<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<a class="navbar-brand" href="#">Title</a>
				<ul class="nav navbar-nav">
					<li>
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Link</a>
					</li>
				</ul>
				 <ul class="nav navbar-nav navbar-right">                    
            <li>
                <a href="<?= base_url(''); ?>">Login</a>
            </li>
            <li>
                <a href="#">Contact</a>
            </li> 
          </ul>
			</div>
		</nav>
		<br>


		<div id="container">
			<h1>Welcome to CodeIgniter!</h1>

			<div id="body">
				<?php
				function exibe_cep($cep, $t) {
				if ($t->viacep->consultar($cep)) {
				?>
				<strong>CEP: </strong><?= $t->viacep->getCEP() ?><br/>
				<strong>Logradouro: </strong><?= $t->viacep->getLogradouro() ?><br/>
				<strong>Complemento: </strong><?= $t->viacep->getComplemento() ?><br/>
				<strong>Bairro: </strong><?= $t->viacep->getBairro() ?><br/>
				<strong>Localidade: </strong><?= $t->viacep->getLocalidade() ?><br/>
				<strong>UF: </strong><?= $t->viacep->getUF() ?><br/>
				<strong>IBGE: </strong><?= $t->viacep->getIBGE() ?><br/>
				<strong>GIA: </strong><?= $t->viacep->getGIA() ?><br/>
				<?php
				} else {
				echo $t->viacep->getUltimoErro();
				}
				echo '<br/><br/>';
				}
				// testa
				exibe_cep('01001-000', $this);
				exibe_cep('01310-909', $this);
				exibe_cep('13465-970', $this);
				?>
			</div>
			<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
		</div>
	


		
		
		
		
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>
</html>